.. GTMAdminOps documentation master file, created by
   sphinx-quickstart on Mon Nov 27 09:22:04 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. ###############################################################
.. #                                                             #
.. # Copyright (c) 2020 YottaDB LLC and/or its subsidiaries.     #
.. # All rights reserved.                                        #
.. #                                                             #
.. #     This source code contains the intellectual property     #
.. #     of its copyright holder(s), and is made available       #
.. #     under a license.  If you do not know the terms of       #
.. #     the license, please stop and do not read further.       #
.. #                                                             #
.. ###############################################################

Welcome to the Administration and Operations Guide!
====================================================

.. toctree::
   :maxdepth: 5

   Main YottaDB Documentation Page <https://yottadb.com/resources/documentation/>
   man
   about
   installydb
   basicops
   gde
   dbmgmt
   ydbjournal
   dbrepl
   mlocks
   gds
   dse
   integrity
   encryption
   gtcm
   ipcresource
   monitoring
   securityph
   dockercontainer
   packaging
   tls
   glossary
   extending
   LICENSE

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
